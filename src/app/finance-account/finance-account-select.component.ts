import {Component, OnInit} from "@angular/core";
import {FinanceAccountService} from "./servcie/finance-account.service";
import {FinanceAccount} from "../transaction/model/finance-account";
import {ModalService} from "../services/modal.service";
import {DataService} from "../common/Service/data-service";

@Component({
  selector: 'finance-Account-select',
  template: `
    {{message}}
      <mat-form-field>
          <mat-select placeholder="Finance Account">
              <mat-option *ngFor="let financeAccount of financeAccounts" [value]="financeAccount.name">
                  {{financeAccount.name}}
              </mat-option>
          </mat-select>
      </mat-form-field>  
      <button mat-mini-fab color="primary" style="float: right; font-size: 22px; color: white" (click)="openAddFinanceAccount()">
          +
      </button>

  `,
  styleUrls: ['./finance-account.component.css']
})
export class FinanceAccountSelect implements OnInit {

  options: string[];
  constructor(public financeAccountService: FinanceAccountService, private modalService: ModalService, private data: DataService) {
  }

  financeAccounts:  FinanceAccount[] =[];

  ngOnInit(): void {
    this.data.currentMessage.subscribe(newFinanceAccount =>  this.financeAccounts.push(newFinanceAccount))
    this.getAllFinanceAccountsNames();

  }

  getAllFinanceAccountsNames() {

    this.financeAccountService.getAllFinanceAccounts().subscribe((data: FinanceAccount[]) => {
      this.financeAccounts = data;
    });
  }
  openAddFinanceAccount() {
    this.modalService.openAddFinanceAccountModal();
  }
}
