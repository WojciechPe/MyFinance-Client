import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Transaction} from "../../transaction/transaction";
import {FinanceAccount} from "../../transaction/model/finance-account";

@Injectable({
  providedIn: 'root'
})
export class FinanceAccountService {

  constructor(private http: HttpClient) { }


 getAllFinanceAccounts()
  {
    return this.http
      .get<FinanceAccount[]>('http://localhost:8080/FinanceAccount/allUserFinanceAccounts');
  }
 saveFinanceAccount(financeAccount)
  {
    console.log('saveFinanceAccount');
    return this.http
      .post<FinanceAccount>('http://localhost:8080/FinanceAccount/addFinanceAccount',financeAccount);
  }
}
