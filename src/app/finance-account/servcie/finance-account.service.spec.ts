import { TestBed } from '@angular/core/testing';

import { FinanceAccountService } from './finance-account.service';

describe('FinanceAccountServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FinanceAccountService = TestBed.get(FinanceAccountService);
    expect(service).toBeTruthy();
  });
});
