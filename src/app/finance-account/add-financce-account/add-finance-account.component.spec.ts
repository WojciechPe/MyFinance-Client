import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFinanceAccountComponent } from './add-finance-account.component';

describe('AddFinancceAccountComponent', () => {
  let component: AddFinanceAccountComponent;
  let fixture: ComponentFixture<AddFinanceAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFinanceAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {

    // @ts-ignore
    fixture = TestBed.createComponent(AddFinanceAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
