import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractModal} from "../../abstract-modal";
import {MatDialog} from "@angular/material/dialog";
import {TransactionService} from "../../transaction/service/transaction.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {FinanceAccountService} from "../servcie/finance-account.service";
import {Transaction} from "../../transaction/transaction";
import {FinanceAccount} from "../../transaction/model/finance-account";
import {User} from "../../common/Model/user";
import {DataService} from "../../common/Service/data-service";

@Component({
  selector: 'app-add-finance-account',
  templateUrl: './add-finance-account.component.html',
  styleUrls: ['./add-finance-account.component.css']
})
export class AddFinanceAccountComponent extends  AbstractModal implements OnInit {

  title: string;
  financeAccount : FinanceAccount;
  financeAccountForm: FormGroup;
  date = new Date();
  constructor(public dialog: MatDialog,public service:FinanceAccountService, private formBuilder: FormBuilder, private data: DataService) {
    super(dialog);
    this.title = 'Add finance Account';
    this.financeAccount = new FinanceAccount();
  }
  message:string;

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
    this.financeAccountForm = this.formBuilder.group({
      'financeAccount_tittle': [null, Validators.required],
      'financeAccount_amount': [null, Validators.required]
    });
    this.financeAccountForm.addControl('date',new FormControl(this.date))
  }

    addFinanceAccount() {
      console.log(this.financeAccount);
      this.service.saveFinanceAccount(this.financeAccount).subscribe(
          (data: FinanceAccount) => {
            this.data.changeMessage(data);
            this.dialog.closeAll();
          },
          response => {
            console.log("POST call in error", response);
          },
          () => {
            console.log("The POST observable is now completed.");
          });
    }
}
