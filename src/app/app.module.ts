import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Oauth2RedirectComponent} from "./Auth/oauth2-redirect.component";
import {TransactionComponent} from './transaction/transaction.component';
import {MatCardModule, MatFormFieldModule, MatInputModule, MatTableModule} from "@angular/material";
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModalsComponent} from './common/comonn-modals/common-modals.component';
import {LoginComponent} from './Auth/login/login.component';
import {RegisterComponent} from "./Auth/register/register.component";
import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AuthInterceptor} from "./Auth/auth-interceptor";
import {AddTransactionComponent} from './transaction/add-transaction/add-transaction.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {DatePipe} from "@angular/common";
import {MatSelectModule} from "@angular/material/select";
import {FinanceAccountComponent} from './finance-account/finance-account.component';
import {FinanceAccountSelect} from "./finance-account/finance-account-select.component";
import { AddFinanceAccountComponent } from './finance-account/add-financce-account/add-finance-account.component';


@NgModule({
  declarations: [
    AppComponent,
    Oauth2RedirectComponent,
    TransactionComponent,
    CommonModalsComponent,
    LoginComponent,
    RegisterComponent,
    AddTransactionComponent,
    FinanceAccountComponent,
    FinanceAccountSelect,
    AddFinanceAccountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatCardModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule
  ],
  providers: [MatDatepickerModule,{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true, },  DatePipe

],
  exports: [],
  bootstrap: [AppComponent],
  entryComponents: [TransactionComponent, LoginComponent,RegisterComponent,AddTransactionComponent,AddFinanceAccountComponent],
})
export class AppModule {
}
platformBrowserDynamic().bootstrapModule(AppModule);
