import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {FinanceAccount} from "../../transaction/model/finance-account";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private messageSource = new BehaviorSubject(new FinanceAccount());
  currentMessage = this.messageSource.asObservable();

  constructor() {
  }

  changeMessage(message: FinanceAccount) {
    this.messageSource.next(message)

  }
}

