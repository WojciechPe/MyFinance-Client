export class Transaction {

  id: string;
  title: string;
  transactionCategory: [];
  transactionType: string;
  note: string;
  amount: number;
  category: string;
  date: any;
}
