import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {TransactionService} from "./service/transaction.service";
import {Transaction} from "./transaction";
import {ModalService} from "../services/modal.service";

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})

export class TransactionComponent  {
  displayedColumns = ['title','date', 'title', 'amount','type'];
 //dataSource = ELEMENT_DATA;
  transactions: Transaction[];
  dataSource = this.transactions;
  constructor(public transactionService: TransactionService, private modalService: ModalService) {
  }

  ngOnInit()
  {
      this.transactionService
          .getTransactions("start","end")
          .subscribe((data:Transaction[]) => {
            this.transactions = data;
          });
  }

  openAddTransaction() {

    this.modalService.openAddTransactionModal();

  }
}



