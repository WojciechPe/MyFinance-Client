import {Component, OnInit} from '@angular/core';
import {AbstractModal} from "../../abstract-modal";
import {MatDialog} from "@angular/material/dialog";
import {Transaction} from "../transaction";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TransactionService} from "../service/transaction.service";
import {DatePipe} from "@angular/common";



@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.css']
})
export class AddTransactionComponent extends AbstractModal implements OnInit {
  title: string;
  transaction : Transaction;
  transactionForm: FormGroup;
  date = new Date();
  constructor(public dialog: MatDialog,service: TransactionService, private formBuilder: FormBuilder,private datepipe:DatePipe) {
    super(dialog);
    this.title = 'Add transaction';
    this.transaction = new Transaction();
  }

  ngOnInit() {

    this.transactionForm = this.formBuilder.group({
      'transaction_title': [null, Validators.required],
      'transaction_amount': [null, Validators.required]
    });
    this.transactionForm.addControl('date',new FormControl(this.date))

    // console.log(this.date);
    // this.transactionForm = new FormGroup({
    //   date:new FormControl(this.date),
    // })

  }





  addTransaction() {
console.log(this.transaction)
  }


}

