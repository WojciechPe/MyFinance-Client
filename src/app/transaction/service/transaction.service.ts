import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../../common/Model/user";
import {Transaction} from "../transaction";
import {catchError} from "rxjs/operators";
import {CommonService} from "../../common/Service/common.service";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient, private  commonService: CommonService) { }

    getTransactions(beginDate : string,endDate: string)
    {


      console.log("getTransactions");
      return this.http
        .get<Transaction[]>('http://localhost:8080/Transaction/getTransactions');
   //.subscribe(
        //   (data: Transaction[]) => {
        //     console.log("get Transactions call successful value returned in body",
        //       data);
        //   },
        //   response => {
        //     console.log("POST call in error", response);
        //   },
        //   () => {
        //     console.log("The POST observable is now completed.");
        //   });
    }

  // getPlanedTransactions(beginDate : string,endDate: string)

  addTransaction()
    {
      return this.http
        .get<User>('http://localhost:8080/tansaction/addTransaction').subscribe(
          (data: User) => {
            console.log("get logged user call successful value returned in body",
              data);
          },
          response => {
            console.log("POST call in error", response);
          },
          () => {
            console.log("The POST observable is now completed.");
          });
    }
}
